---

workflow:
  name: "[$PIPELINE_TYPE] $CI_COMMIT_TITLE"
  rules:
    # Use merge request pipelines
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      variables:
        PIPELINE_TYPE: 'mr'
    # Inhibit double pipelines
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    # Allow branch pipelines on non-master branches if there is no merge request.
    # No need to run branch pipelines on master: images are built and
    # pushed by the merge request pipelines.
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH'
      when: always
      variables:
        PIPELINE_TYPE: 'branch'
    - when: never

default:
  interruptible: true
  tags:
    - saas-linux-2xlarge-amd64

variables:
  # Each image is tagged with the commit
  DOCKER_IMAGE_TAG_SUFFIX: "${CI_COMMIT_SHA}"
  # Caches are fetched from the merge base
  DOCKER_IMAGE_TAG_CACHE_SUFFIX: "${CI_MERGE_REQUEST_DIFF_BASE_SHA}"
  # We also tag each image with the sanitized branch names, and fetch
  # caches therefrom
  DOCKER_IMAGE_TAG_EXTRA: "${CI_COMMIT_REF_SLUG}"

stages:
  - lint
  - build
  - publish
  - mirror

# Match GitLab executors Docker version and directly use the Docker socket
# The Docker daemon is already configured, experimental features are enabled
# The following environment variables are already set:
# - BUILDKIT_PROGRESS
# - DOCKER_DRIVER
# - DOCKER_VERSION
# https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-socket-binding
.image_template__docker:
  # https://gitlab.com/tezos/docker-images/ci-docker
  image: "${CI_REGISTRY}/tezos/docker-images/ci-docker:v1.9.0"
  before_script:
    # Credentials for Docker registries
    - ./scripts/docker_registry_auth.sh

.rules__mavryk_namespace:
  rules:
    - if: '$CI_PROJECT_NAMESPACE == "mavryk-network"'

.rules__not_mavryk_namespace:
  rules:
    - if: '$CI_PROJECT_NAMESPACE != "mavryk-network"'

.rules__protected:
  rules:
    - if: '$AWS_ACCESS_KEY_ID && $AWS_SECRET_ACCESS_KEY && $AWS_ECR'

# Template used by all Docker image build jobs
# Variables needs to be set properly
.build-template:
  stage: build
  extends:
    - .image_template__docker
  services:
    - docker:${DOCKER_VERSION}-dind
  variables:
    # Docker build argument for architecture specific instructions
    TARGETARCH: ""
    # Version of the docker to use, docker daemon and client must
    # have the same version.
    DOCKER_VERSION: "24.0.7"
  script:
    - . ./scripts/version.sh
    - ./scripts/docker_wait_for_daemon.sh
    - ./scripts/docker_check_version.sh ${DOCKER_VERSION}
    # Build docker images
    - ./scripts/create_docker_images.sh "${CI_REGISTRY_IMAGE}" "${DOCKER_IMAGE_TAG_SUFFIX}" "${DOCKER_IMAGE_TAG_CACHE_SUFFIX}" "${DOCKER_IMAGE_TAG_EXTRA}" "${TARGETARCH}"
    # Verify we packed expected software versions
    - ./scripts/check_versions.sh "${CI_REGISTRY_IMAGE}" "${DOCKER_IMAGE_TAG_SUFFIX}" "${TARGETARCH}"
    # Push docker images
    - ./scripts/docker_push_all.sh "${CI_REGISTRY_IMAGE}" "${DOCKER_IMAGE_TAG_SUFFIX}" "${DOCKER_IMAGE_TAG_EXTRA}" "${TARGETARCH}"

# Dockerfile linter
hadolint:
  image: hadolint/hadolint:v2.10.0-debian
  stage: lint
  script:
    - hadolint *Dockerfile

# POSIX shell scripts linter
shellcheck:
  image: koalaman/shellcheck-alpine:v0.9.0
  stage: lint
  script:
    - shellcheck -x scripts/*.sh

# Check shell script formatting with shfmt
shfmt:
  # We use version 3.6.0 which corresponds to the version distributed
  # in Alpine 3.18, and so is the same as used in mavryk-network/mavryk-protocol.
  image: mvdan/shfmt:v3.6.0-alpine
  stage: lint
  script:
    # These are the settings:
    #   (-i(indentation) 2
    #   -sr (space after redirect))
    # that we use in mavryk-network/mavryk-protocol also.
    - shfmt -i 2 -sr -d scripts/*.sh

# Build runtime docker images for x86_64/amd64
build-runtime-amd64:
  extends:
    - .build-template
    - .rules__mavryk_namespace
  variables:
    TARGETARCH: "amd64"

# Build runtime Docker images for arm64/v8
# Only on Nomadic Labs' repositories and arm64 runners
build-runtime-arm64:
  extends:
    - .build-template
    - .rules__mavryk_namespace
  variables:
    TARGETARCH: "arm64"
  tags:
    - saas-linux-large-arm64

# Merge manifests from jobs build-runtime-amd64 and build-runtime-arm64
# Only on Nomadic Labs' repositories
# (Forks don't have the capacity to build arm64 images and thus they don't have the
# capacity to merge the manifest either)
merge-manifests-runtime:
  stage: publish
  extends:
    - .image_template__docker
    - .rules__mavryk_namespace
  script:
    - ./scripts/docker_merge_manifests.sh "${CI_REGISTRY_IMAGE}" "${DOCKER_IMAGE_TAG_SUFFIX}"

# Mirror Docker image from all families from Gitlab container registry to public
# Artifact Registry @GCP and private ECR @AWS, closer to GitLab runners/executors.
# Requires environment variables to be set with AWS credentials and AWS_ECR and
# GCP_REGISTRY.
mirror:
  stage: mirror
  extends:
    - .image_template__docker
    - .rules__protected
  variables:
    AWS_ECR_IMAGE: "${AWS_ECR}/${CI_PROJECT_PATH}"
    GCP_ARTIFACT_REGISTRY_IMAGE: "${GCP_REGISTRY}/${CI_PROJECT_PATH}"
  script:
    - ./scripts/mirror.sh "${CI_REGISTRY_IMAGE}" "${DOCKER_IMAGE_TAG_SUFFIX}" "${AWS_ECR_IMAGE}"
    - ./scripts/mirror.sh "${CI_REGISTRY_IMAGE}" "${DOCKER_IMAGE_TAG_SUFFIX}" "${GCP_ARTIFACT_REGISTRY_IMAGE}"

# Duplicated job to allow forks to build x86_64/amd64 images
build:
  extends:
    - .build-template
    - .rules__not_mavryk_namespace
  variables:
    # this variable is empty as we don't want to merge any manifest
    DOCKER_IMAGE_TAG_SUFFIX: ""
    DOCKER_IMAGE_TAG_CACHE_SUFFIX: ""
