# runtime dependencies
#
# This image includes
# - alpine
# - runtime dependencies (libraries linked at load time of the process)
#
# This image is intended for
# - distributing the Mavryk binaries in
# - building the CI images on top of it in the image stack (see README.md)

ARG BUILD_IMAGE
# hadolint ignore=DL3006
FROM ${BUILD_IMAGE} as runtime-dependencies

# We must re-introduce BUILD_IMAGE in the scope to use it in the label.
ARG BUILD_IMAGE
LABEL org.opencontainers.image.base.name="${BUILD_IMAGE}"

# Use alpine /bin/ash and set shell options
# See https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#run
SHELL ["/bin/ash", "-euo", "pipefail", "-c"]

# Open Container Initiative
# https://github.com/opencontainers/image-spec/blob/main/annotations.md
LABEL org.opencontainers.image.authors="info@mavryk.io" \
      org.opencontainers.image.description="Mavkit - GitLab CI docker image" \
      org.opencontainers.image.documentation="https://protocol.mavryk.org/" \
      org.opencontainers.image.licenses="MIT" \
      org.opencontainers.image.source="https://gitlab.com/mavryk-network/opam-repository" \
      org.opencontainers.image.title="runtime-dependencies" \
      org.opencontainers.image.url="https://gitlab.com/mavryk-network/mavryk-protocol" \
      org.opencontainers.image.vendor="Mavryk Dynamics"

USER root

# Create a static system group and system user (no password + login shell)
# Prepare sudo, ssh and run
RUN echo 'mavryk:x:1000:mavryk' >> /etc/group \
 && echo 'mavryk:x:1000:1000:mavryk:/home/mavryk:/bin/sh' >> /etc/passwd \
 && echo 'mavryk:!::0:::::' >> /etc/shadow \
 && mkdir -pv /home/mavryk/.ssh /run/mavryk/client /run/mavryk/node \
 && chown -R mavryk:mavryk /home/mavryk /run/mavryk \
 && chmod 700 /home/mavryk/.ssh \
 && mkdir -pv /etc/sudoers.d \
 && echo 'mavryk ALL=(ALL:ALL) NOPASSWD:ALL' > /etc/sudoers.d/mavryk \
 && chmod 440 /etc/sudoers.d/mavryk

# Sapling parameters
COPY ./zcash-params/sapling-output.params ./zcash-params/sapling-spend.params /usr/share/zcash-params/

# hadolint ignore=DL3018
RUN apk --no-cache add \
    binutils \
    gcc \
    gmp \
    libgmpxx \
    hidapi \
    libc-dev \
    libev \
    libffi \
    sudo \
    sqlite-libs

USER mavryk
ENV USER=mavryk
WORKDIR /home/mavryk

# runtime + prebuild dependencies
#
# This image builds upon the `runtime-dependencies` image, see its
# header for details on its content.
#
# It adds upon the contents of `runtime-dependencies`:
# - non-opam build-dependencies (rust dependencies)
# - cache for opam build-dependencies
#
# This image is intended for
# - testing the buildability of mavryk opam packages
# - building images on top of it in the image stack (see README.md)

FROM runtime-dependencies as runtime-prebuild-dependencies

LABEL org.opencontainers.image.title="runtime-prebuild-dependencies"

USER root

# SHELL already set in runtime-dependencies

WORKDIR /tmp

# Automatically set if you use Docker buildx
ARG TARGETARCH

# why the git config???
COPY --chown=mavryk:mavryk .gitconfig /home/mavryk/

# hadolint ignore=DL3018,DL3019
RUN apk update \
# Do not use apk --no-cache here because opam needs the cache.
# See https://github.com/ocaml/opam/issues/5186
 && apk add --no-cache \
    autoconf \
    automake \
    bash \
    build-base \
    ca-certificates \
    cargo \
    cmake \
    coreutils \
    curl \
    eudev-dev \
    git \
    gmp-dev \
    jq \
    libev-dev \
    libffi-dev \
    libtool \
    linux-headers \
    m4 \
    ncurses-dev \
    opam \
    openssh-client \
    openssl-dev \
    patch \
    perl \
    postgresql14-dev \
    rsync \
    tar \
    unzip \
    wget \
    xz \
    zlib-dev \
    zlib-static \
    libusb-dev \
    hidapi-dev \
    sccache \
    shfmt \
    upx \
    protobuf \
    protobuf-dev \
    sqlite-static \
    sqlite-dev \
# Cleanup
 && rm -rf /tmp/*

USER mavryk
WORKDIR /home/mavryk

# TODO: Use a single COPY instruction and avoid duplicates files
COPY --chown=mavryk:mavryk repo opam-repository/
COPY --chown=mavryk:mavryk \
      packages/ocaml \
      packages/ocaml-config \
      packages/ocaml-base-compiler \
      packages/ocaml-options-vanilla \
      packages/base-bigarray \
      packages/base-bytes \
      packages/base-unix \
      packages/base-threads \
      opam-repository/packages/

WORKDIR /home/mavryk/opam-repository

# Install OCaml
ARG OCAML_VERSION
# hadolint ignore=SC2046,DL4006
RUN opam init --disable-sandboxing --no-setup --yes \
              --compiler ocaml-base-compiler.${OCAML_VERSION} \
              mavryk /home/mavryk/opam-repository \
 && opam clean

# Add opam cache
COPY --chown=mavryk:mavryk packages packages
RUN opam admin cache \
 && opam update \
 && opam clean

ENTRYPOINT [ "opam", "exec", "--" ]
CMD [ "/bin/sh" ]

# runtime + build dependencies
#
# This image builds upon the `runtime-prebuild-dependencies` image,
# see its header for details on its content.
#
# It removes the `cache for opam build-dependencies` from that image, and adds:
# - opam build-dependencies
#
# This image is intended for
# - building mavryk from source
# - building images on top of it in the image stack (see README.md)

FROM runtime-prebuild-dependencies as runtime-build-dependencies

LABEL org.opencontainers.image.title="runtime-build-dependencies"

# SHELL already set in runtime-dependencies

USER mavryk
WORKDIR /home/mavryk

# Build blst used by ocaml-bls12-381 without ADX to support old CPU
# architectures.
# See https://gitlab.com/tezos/tezos/-/issues/1788 and
# https://gitlab.com/dannywillems/ocaml-bls12-381/-/merge_requests/135/
ENV BLST_PORTABLE=yes

# hadolint ignore=SC2046,DL4006
RUN opam install --yes $(opam list --all --short | grep -v ocaml-option-) \
    && opam clean

# ENTRYPOINT and CMD already set in runtime-prebuild-dependencies

# runtime + build + test dependencies
#
# This image builds upon the `runtime-build-dependencies` image, see
# its header for details on its content.
#
# It adds upon the contents of `runtime-build-dependencies`:
# - shellcheck
# - a python environment for building the mavkit documentation
# - nvm for javascript backend testing
#
# This image is intended for
# - running certain tests in the mavryk-network/mavryk-protocol repo (unit tests, shellcheck)
# - building the documentation
# - building images on top of it in the image stack (see README.md)


FROM runtime-build-dependencies as runtime-build-test-dependencies

LABEL org.opencontainers.image.title="runtime-build-test-dependencies"

# SHELL already set in runtime-dependencies

USER root

WORKDIR /tmp

# hadolint ignore=DL3018,SC2046
RUN apk --no-cache add \
        python3-dev \
        poetry \
        shellcheck

USER mavryk
WORKDIR /home/mavryk

### Javascript env setup as mavryk user

# TODO: https://gitlab.com/tezos/tezos/-/issues/5026
# The js dependencies could be downloaded from mavryk-network/mavryk-protocol and installed
# here.

COPY --chown=mavryk:mavryk nodejs/install-nvm.sh /tmp/install-nvm.sh
RUN /tmp/install-nvm.sh \
 && rm -rf /tmp/*

### Python setup

# Required to have poetry in the path in the CI
ENV PATH="/home/mavryk/.local/bin:${PATH}"

# Copy poetry files to install the dependencies in the docker image
COPY --chown=mavryk:mavryk ./poetry.lock ./pyproject.toml ./

# Poetry will create the virtual environment in $(pwd)/.venv.
# The containers running this image can load the virtualenv with
# $(pwd)/.venv/bin/activate and do not require to run `poetry install`
# It speeds up the Mavryk CI and simplifies the .gitlab-ci.yml file
# by avoiding duplicated poetry setup checks.
RUN poetry config virtualenvs.in-project true \
 && poetry install \
 && rm -rf /tmp/* /home/mavryk/.cache/pip /home/mavryk/.cache/pypoetry

# ENTRYPOINT and CMD already set in runtime-prebuild-dependencies

# runtime + end-to-end test dependencies
#
# This image builds on the runtime dependencies and additionally includes:
#
# - ocamlformat and bisect-ppx-report, copied from the
#   runtime-build-dependencies image.
# - eth-cli, installed from npm
# - some utilities used in the Tezt integration test suite: curl, git, file
# - some utilities used in the [unified_coverage] job: make, jq
#
# This image is intended for
# - Running end-to-end tests (mostly Tezt) in the mavryk-network/mavryk-protocol CI.

ARG OCAML_IMAGE
ARG BUILD_IMAGE

# hadolint ignore=DL3006
FROM runtime-dependencies as runtime-e2etest-dependencies

LABEL org.opencontainers.image.title="runtime-e2etest-dependencies"

USER root

# SHELL already set in runtime-dependencies

WORKDIR /tmp

# Automatically set if you use Docker buildx
ARG TARGETARCH

# Retrieve ocamlformat, used in the snoop tests.
ARG OCAML_VERSION
COPY --from=runtime-build-dependencies \
    /home/mavryk/.opam/ocaml-base-compiler.${OCAML_VERSION}/bin/ocamlformat \
    /home/mavryk/.opam/ocaml-base-compiler.${OCAML_VERSION}/bin/bisect-ppx-report \
    /bin/

# TODO: https://gitlab.com/tezos/tezos/-/issues/5026
# We could install npm via nvm if we tackle this issue.
# In the meantime, removes nvm installed in runtime-build-test-dependencies and
# install npm via apk.

# Fixing some ipv6 issues on the runner. Always prioritizing ipv4
ENV NODE_OPTIONS="--dns-result-order=ipv4first"

# We need curl since a bunch of tezt tests use curl.
# Same, some tests use [file].

# hadolint ignore=DL3018,DL3019
RUN apk update \
 && apk add --no-cache curl npm git file make jq \
 # We need eth since e2e tests. This requires git as well.
 && npm install -g eth-cli@2.0.2 \
 # clean up
 && rm -rf /tmp/*

USER mavryk
WORKDIR /home/mavryk
