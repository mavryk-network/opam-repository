opam-version: "2.0"
maintainer: "Jane Street developers"
authors: ["Jane Street Group, LLC"]
homepage: "https://github.com/janestreet/ppx_assert"
bug-reports: "https://github.com/janestreet/ppx_assert/issues"
dev-repo: "git+https://github.com/janestreet/ppx_assert.git"
doc: "https://ocaml.janestreet.com/ocaml-core/latest/doc/ppx_assert/index.html"
license: "MIT"
build: [
  ["dune" "build" "-p" name "-j" jobs]
]
depends: [
  "ocaml"         {>= "4.14.0"}
  "base"          {>= "v0.16" & < "v0.17"}
  "ppx_cold"      {>= "v0.16" & < "v0.17"}
  "ppx_compare"   {>= "v0.16" & < "v0.17"}
  "ppx_here"      {>= "v0.16" & < "v0.17"}
  "ppx_sexp_conv" {>= "v0.16" & < "v0.17"}
  "dune"          {>= "2.0.0"}
  "ppxlib"        {>= "0.28.0"}
]
synopsis: "Assert-like extension nodes that raise useful errors on failure"
description: "
Part of the Jane Street's PPX rewriters collection.
"
url {
  src:
    "https://ocaml.janestreet.com/ocaml-core/v0.16/files/ppx_assert-v0.16.0.tar.gz"
  checksum: [
    "sha256=57dc6e241827eb1d5112c958f2f682ddd0addf5a8e9d589f5361ec2669883fd5"
    "sha512=addd76b0bc936d740c755377974d0c8f49a508c141ebf78ec1ab42cc28595ad2d6d59f172720d677b21eaf24fab83b99f7d12ec7b5bc2c421df5016b9c6259a7"
  ]
}
